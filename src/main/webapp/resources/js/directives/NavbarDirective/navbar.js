angular.module('ElectronicStoreApp.navbar',[]).
    directive('navbar', function($state, $cookieStore, $interval, $http){
    return{
        restrict:'E',
        scope : {

        },
        controller:function($scope, $rootScope){
            $scope.customer = {};

            $scope.logout = function(){
                $cookieStore.remove('id');
                $cookieStore.remove('user');
                $cookieStore.remove('adminId');
                $cookieStore.remove('admin');
                $rootScope.mainAdmin = null;
                $scope.mainUser = null;
                $scope.shoppingCart = {};
                $rootScope.loggedIn = false;
                $state.go('landingPage');
            };

            $scope.categoryPage = function (category) {
                $state.go('productCategory', {
                    productCategory:category
                });

                console.log(category);
            };

            $scope.profilePage = function () {
                $state.go('profile', {
                    customerId:$scope.mainUser.id
                });
            };

            $scope.checkout = function () {
                $state.go('productCheckout', {
                    customerId:$scope.mainUser.id
                });
            };

            $scope.mainUser = $cookieStore.get('user');
            if($cookieStore.get('id')){
                $rootScope.loggedIn = true;
            }

            $rootScope.mainAdmin = $cookieStore.get('admin');
            if($cookieStore.get('adminId')){
                $rootScope.loggedIn = true;
            }

            $scope.loginCustomer = function () {
                if($scope.customer.email!=null && $scope.customer.password!=null){
                    $http.post('/api/restcustomercontroller/loginCustomer', JSON.stringify($scope.customer))
                        .success(function (data, status) {
                            if (status == 200) {
                                $scope.loggedInCustomer = data;
                                $cookieStore.put('id',$scope.loggedInCustomer[0].id);
                                $cookieStore.put('user',$scope.loggedInCustomer[0]);
                                $rootScope.loggedIn = true;
                                $state.go('landingPage');
                                $scope.mainUser = $scope.loggedInCustomer[0];
                            }
                        })
                        .error(function (error) {
                            alert("An error has occurred");
                        });
                }
            };

            $scope.shoppingCartInterval = function () {
                $scope.promise = $interval(function() {
                    $rootScope.checkShoppingCart();
                }, 8000);
            };
            $scope.shoppingCartInterval();

            $rootScope.checkShoppingCart = function () {

                if($cookieStore.get('id')){
                    $scope.mainUser = $cookieStore.get('user');
                    $http.post('/api/restpurchasecontroller/getCustomerShoppingCart', JSON.stringify($scope.mainUser.id))
                        .success(function (data, status) {
                            if (status == 200) {
                                $scope.shoppingCart = data;
                                console.log(data, "ShoppingCart");
                                $scope.calculateCartSubtotal();
                            }
                        })
                        .error(function (error) {
                            alert("An error has occurred");
                        });
                }
            };

            $rootScope.checkShoppingCart();

            $scope.removeFromShoppingCart = function (item) {
                delete item['$$hashKey'];
                $http.post('/api/restpurchasecontroller/deletePurchase', JSON.stringify(item))
                    .success(function (data, status) {
                        if (status == 200) {
                            $rootScope.checkShoppingCart();
                        }
                    })
                    .error(function (error) {
                        alert("An error has occurred");
                    });

            };

            $scope.storeElement = function (element) {
                $scope.element = element;
            };

            $scope.newProduct = {};
            $scope.addNewProduct = function (element) {
                if($scope.newProduct!=null) {
                    delete $scope.newProduct['$$hashKey'];
                    $http.post('/api/reststockcontroller/createStock', JSON.stringify($scope.newProduct))
                        .success(function (data, status) {
                            if (status == 200) {
                                $scope.newProduct = data;
                                if($scope.element!=null) {
                                    $scope.fileStockPhotoSelected($scope.element);
                                }
                            }
                        })
                        .error(function (error) {
                            alert("An error has occurred");
                        });
                }
            };

            $scope.purchasePayment = function () {
                if($scope.shoppingCart!=null) {
                    for(var i = 0; i < $scope.shoppingCart.length; i++){
                        delete $scope.shoppingCart[i]['$$hashKey'];
                    }
                    $http.post('/api/restpurchasecontroller/purchasePayment', JSON.stringify($scope.shoppingCart))
                        .success(function (data, status) {
                            if (status == 200) {
                            }
                        })
                        .error(function (error) {
                            alert("An error has occurred");
                        });
                }
            };

            $scope.calculateCartSubtotal = function () {
                var total = 0;

                for(var i = 0; i < $scope.shoppingCart.length; i++){
                    total += $scope.shoppingCart[i].quantity * $scope.shoppingCart[i].stock.price;
                }
                $scope.subtotalBeforeDiscount = total;
                $scope.discount = 0;
                if($scope.mainUser.subscriber){
                    $scope.subtotalAfterDiscount = total * 0.95;
                    $scope.discount = total * .05;
                }else{
                    $scope.subtotalAfterDiscount = total;
                }
            };

            $scope.fileStockPhotoSelected = function (element) {
                var file = element.files[0];
                var storageRef = firebase.storage().ref('products/'+$scope.newProduct.id+'/photos/'+ file.name);
                var state = storageRef.put(file);
                state.on('state_changed',
                    function progress(snapshot) {
                        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                        console.log('Upload is ' + progress + '% done');
                        switch (snapshot.state) {
                            case firebase.storage.TaskState.PAUSED: // or 'paused'
                                console.log('Upload is paused');
                                break;
                            case firebase.storage.TaskState.RUNNING: // or 'running'
                                console.log('Upload is running');
                                break;
                        }
                    },

                    function error(err) {
                        switch (error.code) {
                            case 'storage/unauthorized':
                                // User doesn't have permission to access the object
                                break;

                            case 'storage/canceled':
                                // User canceled the upload
                                break;

                            case 'storage/unknown':
                                // Unknown error occurred, inspect error.serverResponse
                                break;
                        }

                    },

                    function complete() {
                        var StockPhoto = state.snapshot.downloadURL;
                        $scope.newProduct.stockImageUrl = StockPhoto;
                        if($scope.newProduct!=null) {
                            delete $scope.newProduct['$$hashKey'];
                            $http.post('/api/reststockcontroller/updateStock', JSON.stringify($scope.newProduct))
                                .success(function (data, status) {
                                    if (status == 200) {
                                        console.log($scope.newProduct, "New complete product");
                                        $state.go('product', {
                                            productId: $scope.newProduct.id,
                                            productTitle: $scope.newProduct.title
                                        });
                                    }
                                })
                                .error(function (error) {
                                    alert("An error has occurred");
                                });
                        }
                    }
                )
            };
        },
        templateUrl:'resources/js/directives/NavbarDirective/navbar.html',
        transclude: false
    }
});
