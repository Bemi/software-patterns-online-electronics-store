angular.module('ElectronicStoreApp.LandingPageController', ['ElectronicStoreApp.IteratorService']).
    controller('LandingPageController', function ($scope, $state, $http, IteratorService) {

        $http.get('/api/reststockcontroller/getAllStocks')
            .success(function (data, status) {
                if (status == 200) {
                    $scope.allStocks = [];
                    var iter = new IteratorService.Iterator(data);
                    for (var item = iter.first(); iter.hasNext(); item = iter.next()) {
                        $scope.allStocks.push(item)
                    }
                }
            })
            .error(function (error) {
                alert("An error has occurred");
            });

        $scope.goToProduct = function (product) {
            $state.go('product', {
                productId:product.id,
                productTitle:product.title
            });
        };


});
