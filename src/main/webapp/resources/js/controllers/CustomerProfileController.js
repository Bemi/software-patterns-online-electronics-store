angular.module('ElectronicStoreApp.CustomerProfileController', []).
    controller('CustomerProfileController', function ($scope, $state, $http, $stateParams, $cookieStore, $rootScope) {

        var customerId = $stateParams.customerId;

        $scope.getCustomerInfo = function() {
            if(customerId!=null){
                $http.post('/api/restcustomercontroller/findCustomerById',customerId)
                    .success(function (data, status) {
                        if (status == 200) {

                            $scope.profileDetails = data;
                        }
                    })
                    .error(function (error) {
                        alert("An error has occurred");
                    });
            }
        };

        $scope.getCustomerInfo();

        $scope.updateCustomer = function() {
            if($scope.profileDetails!=null){
                $http.post('/api/restcustomercontroller/updateCustomer',JSON.stringify($scope.profileDetails))
                    .success(function (data, status) {
                        if (status == 200) {
                            $scope.loggedInCustomer = data;
                            $cookieStore.put('id',$scope.loggedInCustomer.id);
                            $cookieStore.put('user',$scope.loggedInCustomer);
                            $rootScope.loggedIn = true;
                            $scope.mainUser = $scope.loggedInCustomer;
                        }
                    })
                    .error(function (error) {
                        alert("An error has occurred");
                    });
            }
        };

});
