angular.module('ElectronicStoreApp.AdminLoginController', []).
    controller('AdminLoginController', function ($scope, $state, $http, $cookieStore, $location, $interval, $rootScope) {

        $scope.admin = {};
        $scope.loginAdmin = function () {
            if($scope.admin.email!=null && $scope.admin.password!=null){
                $http.post('/api/restadministratorcontroller/loginAdmin', JSON.stringify($scope.admin))
                    .success(function (data, status) {
                        if (status == 200) {
                            $scope.loggedInAdmin = data;
                            $cookieStore.put('adminId',$scope.loggedInAdmin[0].id);
                            $cookieStore.put('admin',$scope.loggedInAdmin[0]);
                            $rootScope.loggedIn = true;
                            $rootScope.mainAdmin = $scope.loggedInAdmin[0];
                            $state.reload();
                        }
                    })
                    .error(function (error) {
                        alert("An error has occurred");
                    });
            }
        };
});
