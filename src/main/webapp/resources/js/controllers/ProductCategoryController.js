angular.module('ElectronicStoreApp.ProductCategoryController', ['ElectronicStoreApp.IteratorService']).
    controller('ProductCategoryController', function ($scope, $state, $http, $cookieStore, $stateParams, firebase, IteratorService) {

        var productCategory = $stateParams.productCategory;
        $scope.getStockByCategory = function () {
            $http.post('/api/reststockcontroller/getStockByCategory', productCategory)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.products = [];
                        var iter = new IteratorService.Iterator(data);
                        for (var item = iter.first(); iter.hasNext(); item = iter.next()) {
                            $scope.products.push(item)
                        }
                    }
                })
                .error(function (error) {
                    alert("An error has occurred");
                });
        };

        $scope.getStockByCategory();

        $scope.goToProduct = function (product) {
            $state.go('product', {
                productId:product.id,
                productTitle:product.title
            });
        };

        $scope.editProduct = function (product) {
            $scope.updateProduct = product;
        };

        $scope.editExisitingProduct = function () {
            if($scope.updateProduct!=null) {
                delete $scope.updateProduct['$$hashKey'];
                $http.post('/api/reststockcontroller/updateStock', JSON.stringify($scope.updateProduct))
                    .success(function (data, status) {
                        if (status == 200) {
                            $state.reload();
                        }
                    })
                    .error(function (error) {
                        alert("An error has occurred");
                    });
            }
        };

        $scope.fileStockPhotoSelected = function (element) {
            var file = element.files[0];
            var storageRef = firebase.storage().ref('products/'+$scope.updateProduct.id+'/photos/'+ file.name);
            var state = storageRef.put(file);
            state.on('state_changed',
                function progress(snapshot) {
                    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    console.log('Upload is ' + progress + '% done');
                    switch (snapshot.state) {
                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                            console.log('Upload is paused');
                            break;
                        case firebase.storage.TaskState.RUNNING: // or 'running'
                            console.log('Upload is running');
                            break;
                    }
                },

                function error(err) {
                    switch (error.code) {
                        case 'storage/unauthorized':
                            // User doesn't have permission to access the object
                            break;

                        case 'storage/canceled':
                            // User canceled the upload
                            break;

                        case 'storage/unknown':
                            // Unknown error occurred, inspect error.serverResponse
                            break;
                    }

                },

                function complete() {
                    var StockPhoto = state.snapshot.downloadURL;
                    $scope.updateProduct.stockImageUrl = StockPhoto;
                }
            )
        };

});
