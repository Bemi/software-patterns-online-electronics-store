angular.module('ElectronicStoreApp.ProductController', ['ElectronicStoreApp.IteratorService']).
    controller('ProductController', function ($scope, $state, $http, $cookieStore, $stateParams, $interval, $rootScope, IteratorService) {

        var productId = $stateParams.productId;
        $scope.getProduct = function () {
            $http.post('/api/reststockcontroller/getStockById', productId)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.product = data;
                        $scope.getStatsOnProduct();
                    }
                })
                .error(function (error) {
                    alert("An error has occurred");
                });
        };

        $scope.getProduct();

        $scope.getAllReviews = function () {
            $http.post('/api/restreviewcontroller/getReviewById', productId)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.reviews = [];
                        var iter = new IteratorService.Iterator(data);
                        for (var item = iter.first(); iter.hasNext(); item = iter.next()) {
                            $scope.reviews.push(item)
                        }
                    }
                })
                .error(function (error) {
                    alert("An error has occurred");
                });
        };

        $scope.getAllReviews();

        $scope.overviewMode = true;

            $scope.tabMode = function (x) {
                if(x==1){
                 $scope.reviewMode = false;
                 $scope.overviewMode = true;
                 $scope.statMode = false;
                }
                if(x==2){
                    $scope.reviewMode = true;
                    $scope.overviewMode = false;
                    $scope.statMode = false;
                }
                if(x==3){
                    $scope.reviewMode = false;
                    $scope.overviewMode = false;
                    $scope.statMode = true;
                }
            };

        $scope.item = {};
        $scope.mainUser = $cookieStore.get('user');
        $scope.customerProductReview ={};
        $scope.rating = 3;

        $scope.addToCart = function () {

            $scope.item.stock = $scope.product;
            $scope.item.customer = $scope.mainUser;
            $scope.item.paid = false;

            if($cookieStore.get('id') && $scope.item.quantity>=1) {
                $scope.mainUser = $cookieStore.get('user');
                $http.post('/api/restpurchasecontroller/createPurchase', JSON.stringify($scope.item))
                    .success(function (data, status) {
                        if (status == 200) {
                            $rootScope.checkShoppingCart();
                        }
                    })
                    .error(function (error) {
                        alert("An error has occurred");
                    });
            }else{
                alert("Please login or Register");
            }
        };


        $scope.postAReview = function(){
            if($scope.mainUser!=null){
                $scope.customerProductReview.stock = $scope.product;
                $scope.customerProductReview.customer = $scope.mainUser;
                $http.post('/api/restreviewcontroller/createReview', JSON.stringify($scope.customerProductReview))
                    .success(function (data, status) {
                        if (status == 200) {
                            $scope.getAllReviews();
                        }
                    })
                    .error(function (error) {
                        alert("An error has occurred");
                    });
            }
        };

        $scope.getStatsOnProduct = function(){
            $http.post('/api/restpurchasecontroller/getPurchasesByStock', JSON.stringify($scope.product))
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.purchasesWithStock = data;
                        $scope.labels = [$scope.product.title];
                        // $scope.series = ['In Stock', 'Sold'];
                        //
                        $scope.soldCount = 0;
                        for(var i = 0; i < $scope.purchasesWithStock.length; i++){
                            $scope.soldCount += $scope.purchasesWithStock[i].quantity;
                        }
                        // $scope.data = [
                        //     [$scope.product.stockLevel],
                        //     [10]
                        // ];

                        // $scope.labels = ['2006'];
                        $scope.series = ['In Stock', 'Sold'];
                        console.log($scope.product.stockLevel);
                        $scope.data = [
                            [$scope.product.stockLevel],
                            [$scope.soldCount]
                        ];
                    }
                })
                .error(function (error) {
                    alert("An error has occurred");
                });

        };
});
