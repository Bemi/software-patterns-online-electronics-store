angular.module('ElectronicStoreApp', [
    'ngRoute',
    'ngSanitize',
    'ui.router',
    'ngCookies',
    'chart.js',
    'firebase',
    'jkAngularRatingStars',
    'ElectronicStoreApp.navbar',
    'ElectronicStoreApp.LandingPageController',
    'ElectronicStoreApp.ProductController',
    'ElectronicStoreApp.AdminLoginController',
    'ElectronicStoreApp.CustomerProfileController',
    'ElectronicStoreApp.ProductCategoryController'
]).
config(function($stateProvider, $urlRouterProvider) {

    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/home/products");
    //
    // Now set up the states
    $stateProvider
        .state('landingPage', {
            url: "/home/products",
            templateUrl: "resources/js/views/landingPage.html",
            controller:'LandingPageController'
        })
        .state('product', {
            url: "/product/:productId/:productTitle",
            templateUrl: "resources/js/views/product.html",
            controller:'ProductController'
        })
        .state('productCategory', {
            url: "/products/category/:productCategory",
            templateUrl: "resources/js/views/productCategory.html",
            controller:'ProductCategoryController'
        })
        .state('profile', {
            url: "/account/:customerId/",
            templateUrl: "resources/js/views/customerProfile.html",
            controller:'CustomerProfileController'
        });

    var firebaseConfig = {
        apiKey: "AIzaSyA-ioKq_7RD-OFuReU4aVgJXt5IP6-PyoM",
        authDomain: "onlinestore-c2bd0.firebaseapp.com",
        databaseURL: "https://onlinestore-c2bd0.firebaseio.com",
        storageBucket: "onlinestore-c2bd0.appspot.com",
        messagingSenderId: "140659804394"
    };
    firebase.initializeApp(firebaseConfig);
}).
run(function($rootScope){
    $rootScope.$on('$stateChangeSuccess', function() {
        $("html, body").animate({ scrollTop: 0 }, 200);
    });
});

