package com.example.controllers;

import com.example.core.entities.Administrator;
import com.example.core.entities.Customer;
import com.example.core.service.IAdministratorService;
import com.example.core.service.impl.AdministratorService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/restadministratorcontroller")
public class RestAdministratorController {

    @RequestMapping(value = "/getAllAdministrators", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Administrator> getAllAdministrators() {

       return AdministratorService.getInstance().findAll();
    }


    @RequestMapping(value = "/createAdministrator", method = RequestMethod.POST, produces = "application/json")
    public void createAdministrator(@RequestBody String administratorJson){

        Administrator administrator = mapAdministrator(administratorJson);

        if (administrator != null) {
            AdministratorService.getInstance().createAdministrator(administrator);
        }

    }

    @RequestMapping(value = "/loginAdmin", method = RequestMethod.POST, produces = "application/json")
    public ArrayList<Administrator> loginCustomer(@RequestBody String emailPasswordJson){


        ArrayList<Administrator> loginAdministrator = new ArrayList<Administrator>();
        Map<String, String> map = new HashMap<String, String>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            map = mapper.readValue(emailPasswordJson, new TypeReference<HashMap<String, String>>() {
            });
            String email = (String) map.get("email");
            String password = (String) map.get("password");
            Administrator administrator = AdministratorService.getInstance().findByEmailAndPassword(email, password);
            if(administrator != null){
                loginAdministrator.add(administrator);
                return loginAdministrator;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loginAdministrator;
    }

    private Administrator mapAdministrator(String administratorJson) {
        Administrator administrator = null;
        try {
            administrator = new ObjectMapper().readValue(administratorJson, Administrator.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return administrator;
    }
}
