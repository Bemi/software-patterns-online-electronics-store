package com.example.controllers;

import com.example.core.entities.Stock;
import com.example.patterns.observer.concrete.SubscriberObserver;
import com.example.patterns.observer.Subject;
import com.example.core.service.IStockService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/reststockcontroller")
public class RestStockController {

    IStockService iStockService;

    @Autowired
    public void setIStockService(IStockService iStockService) {
        this.iStockService = iStockService;
    }

    @RequestMapping(value = "/getAllStocks", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Stock> getAllStocks() {

       return iStockService.findAll();
    }

    @RequestMapping(value = "/getStockById", method = RequestMethod.POST, produces = "application/json")
    public Stock getStockById(@RequestBody String stockId){

        Stock stock = iStockService.findOneById(Integer.parseInt(stockId));

        if (stock != null) {
           return stock;
        }

        return null;
    }

    @RequestMapping(value = "/getStockByCategory", method = RequestMethod.POST, produces = "application/json")
    public List<Stock> getStockByCategory(@RequestBody String stockCategory){

        List<Stock> stocks = iStockService.findByCategory(stockCategory);

        if (stocks != null) {
            return stocks;
        }

        return null;
    }

    @RequestMapping(value = "/createStock", method = RequestMethod.POST, produces = "application/json")
    public Stock createStock(@RequestBody String stockJson){

        Stock stock = mapStock(stockJson);

        if (stock != null) {
            iStockService.createStock(stock);
            Subject subject = new Subject();
            new SubscriberObserver(subject);
            subject.setStock(stock);
            return stock;
        }

        return null;
    }

    @RequestMapping(value = "/updateStock", method = RequestMethod.POST, produces = "application/json")
    public void updateStock(@RequestBody String stockJson){

        Stock stock = mapStock(stockJson);

        if (stock != null) {
            iStockService.updateStock(stock);
        }

    }

    private Stock mapStock(String stockJson) {
        Stock stock = null;
        try {
            stock = new ObjectMapper().readValue(stockJson, Stock.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stock;
    }
}
