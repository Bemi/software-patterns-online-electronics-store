package com.example.controllers;

import com.example.core.entities.Review;
import com.example.core.entities.Stock;
import com.example.core.service.IReviewService;
import com.example.core.service.IStockService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/restreviewcontroller")
public class RestReviewController {

    IReviewService iReviewService;
    IStockService iStockService;

    @Autowired
    public void setIReviewService(IReviewService iReviewService) {
        this.iReviewService = iReviewService;
    }

    @Autowired
    public void setIStockService(IStockService iStockService) {
        this.iStockService = iStockService;
    }

    @RequestMapping(value = "/getAllReviews", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getAllReviews() {

       return "Reviews";
    }
    @RequestMapping(value = "/getReviewById", method = RequestMethod.POST, produces = "application/json")
    public List<Review> getReviewById(@RequestBody String stockId){

        Stock stock = iStockService.findOneById(Integer.parseInt(stockId));

        if (stock != null) {
            List<Review> reviews = iReviewService.findByStock(stock);
            return reviews;
        }

        return null;
    }

    @RequestMapping(value = "/createReview", method = RequestMethod.POST, produces = "application/json")
    public void createReview(@RequestBody String reviewJson){

        Review review = mapCustomer(reviewJson);

        if (review != null) {
            iReviewService.createReview(review);
            Stock stock = review.getStock();
            List<Review> reviews = iReviewService.findAll();
            int count = 0;
            int currentAverage = 0;
            for(Review r :reviews){
                if(r.getStock().getId() == stock.getId()){
                    count++;
                    currentAverage += r.getRating();
                }
            }
            int average = currentAverage/count;
            stock.setAverageRating(average);
            iStockService.updateStock(stock);
        }
    }

    private Review mapCustomer(String reviewJson) {
        Review review = null;
        try {
            review = new ObjectMapper().readValue(reviewJson, Review.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return review;
    }

}