package com.example.controllers;

import com.example.core.entities.Customer;
import com.example.core.service.ICustomerService;
import com.example.core.service.impl.CustomerService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/restcustomercontroller")
public class RestCustomerController {


    @RequestMapping(value = "/getAllCustomers", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Customer> getAllCustomers() {

       return CustomerService.getInstance().findAll();
    }

    @RequestMapping(value = "/createCustomer", method = RequestMethod.POST, produces = "application/json")
    public void createCustomer(@RequestBody String customerJson){

        Customer customer = mapCustomer(customerJson);

        if (customer != null) {
            CustomerService.getInstance().createCustomer(customer);
        }

    }

    @RequestMapping(value = "/findCustomerById", method = RequestMethod.POST, produces = "application/json")
    public Customer findCustomerById(@RequestBody String customerid){

        Customer customer = CustomerService.getInstance().findOneById(Integer.parseInt(customerid));

        return customer;
    }

    @RequestMapping(value = "/updateCustomer", method = RequestMethod.POST, produces = "application/json")
    public Customer updateCustomer(@RequestBody String customerJson){

        Customer customer = mapCustomer(customerJson);

        if (customer != null) {
            CustomerService.getInstance().updateCustomer(customer);
            return customer;
        }
        return null;
    }

    @RequestMapping(value = "/loginCustomer", method = RequestMethod.POST, produces = "application/json")
    public ArrayList<Customer> loginCustomer(@RequestBody String emailPasswordJson){


        ArrayList<Customer> loginCustomer = new ArrayList<Customer>();
        Map<String, String> map = new HashMap<String, String>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            map = mapper.readValue(emailPasswordJson, new TypeReference<HashMap<String, String>>() {
            });
            String email = (String) map.get("email");
            String password = (String) map.get("password");
            Customer customer = CustomerService.getInstance().findByEmailAndPassword(email, password);
            if(customer != null){
                loginCustomer.add(customer);
                return loginCustomer;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loginCustomer;
    }

    private Customer mapCustomer(String customerJson) {
        Customer customer = null;
        try {
            customer = new ObjectMapper().readValue(customerJson, Customer.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return customer;
    }
}
