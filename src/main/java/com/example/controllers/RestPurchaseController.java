package com.example.controllers;

import com.example.core.entities.Customer;
import com.example.core.entities.Purchase;
import com.example.core.entities.Stock;
import com.example.core.service.IPurchaseService;
import com.example.core.service.IStockService;
import com.example.core.service.impl.CustomerService;
import com.example.patterns.strategy.IApplyDiscountStrategy;
import com.example.patterns.strategy.impl.ApplyDiscountStrategy;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/restpurchasecontroller")
public class RestPurchaseController {


    IPurchaseService iPurchaseService;
    IStockService iStockService;

    @Autowired
    public void setIStockService(IStockService iStockService) {
        this.iStockService = iStockService;
    }

    @Autowired
    public void setIPurchaseService(IPurchaseService iPurchaseService) {
        this.iPurchaseService = iPurchaseService;
    }

    @RequestMapping(value = "/getAllPurchases", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getAllPurchases() {

       return "Purchases";
    }

    @RequestMapping(value = "/getCustomerShoppingCart", method = RequestMethod.POST, produces = "application/json")
    public List<Purchase> getCustomerShoppingCart(@RequestBody String customerId){
        Customer customer = CustomerService.getInstance().findOneById(Integer.parseInt(customerId));
        List<Purchase> purchases = iPurchaseService.findByCustomer(customer);
        List<Purchase> shoppingCart = new ArrayList<>();
        if (purchases != null) {
            for(Purchase p: purchases){
                if(!p.isPaid()){
                    shoppingCart.add(p);
                }
            }
            return shoppingCart;
        }
        return null;
    }

    @RequestMapping(value = "/createPurchase", method = RequestMethod.POST, produces = "application/json")
    public void createPurchase(@RequestBody String purchaseJson) {
        Purchase purchase = mapPurchase(purchaseJson);

        iPurchaseService.createPurchase(purchase);
    }

    @RequestMapping(value = "/deletePurchase", method = RequestMethod.POST, produces = "application/json")
    public void deletePurchase(@RequestBody String purchaseJson) {
        Purchase purchase = mapPurchase(purchaseJson);

        iPurchaseService.deletePurchase(purchase);
    }

    @RequestMapping(value = "/purchasePayment",method = RequestMethod.POST, produces = "application/json")
    public void purchasePayment(@RequestBody String purchasesJson){

        List<Purchase> purchases = null;
        try {
            purchases = mapMultiPurchases(purchasesJson);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Customer customer = CustomerService.getInstance().findOneById(purchases.get(0).getCustomer().getId());
        for(Purchase purchase: purchases) {
            Stock stock = purchase.getStock();

            double oldPrice = purchase.getQuantity() * Double.parseDouble(purchase.getStock().getPrice());


            if(customer.isSubscriber()){
                System.out.println("price before discount is "+ stock.getPrice());

                IApplyDiscountStrategy discountStrategy = new ApplyDiscountStrategy();
                double newPrice = discountStrategy.applyDiscount(oldPrice);
                purchase.setPricePaid(newPrice);
            }else{
                purchase.setPricePaid(purchase.getQuantity() * Double.parseDouble(purchase.getStock().getPrice()));
            }
            stock.setStockLevel(stock.getStockLevel()-purchase.getQuantity());
            iStockService.updateStock(stock);
            purchase.setPaid(true);
            iPurchaseService.createPurchase(purchase);
        }
    }

    @RequestMapping(value = "/getPurchasesByStock",method = RequestMethod.POST, produces = "application/json")
    public List<Purchase> getPurchasesByStock(@RequestBody String stockJson){
        Stock stock = iStockService.findOneById(mapStock(stockJson).getId());
        List<Purchase> purchases = iPurchaseService.findByStock(stock).stream().filter(Purchase::isPaid).collect(Collectors.toList());
        return purchases;
    }


    private Purchase mapPurchase(String purchaseJson) {
        Purchase purchase = null;
        try {
            purchase = new ObjectMapper().readValue(purchaseJson, Purchase.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return purchase;
    }

    private List<Purchase> mapMultiPurchases(String purchasesJson) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        List<Purchase> purchases = mapper.readValue(purchasesJson, new TypeReference<List<Purchase>>(){});
        return purchases;
    }

    private Stock mapStock(String stockJson) {
        Stock stock = null;
        try {
            stock = new ObjectMapper().readValue(stockJson, Stock.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stock;
    }
}
