package com.example.core.service;

import com.example.core.entities.Customer;
import com.example.core.entities.Purchase;
import com.example.core.entities.Stock;

import java.util.List;

public interface IPurchaseService {

    List<Purchase> findAll();

    Purchase findOneById(Integer id);

    List<Purchase> findByStock(Stock stock);

    List<Purchase> findByCustomer(Customer customer);

    void createPurchase(Purchase purchase);

    void updatePurchase(Purchase purchase);

    void deletePurchase(Purchase purchase);
}
