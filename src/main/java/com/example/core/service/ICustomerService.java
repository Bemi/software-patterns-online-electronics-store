package com.example.core.service;

import com.example.core.entities.Customer;

import java.util.List;

public interface ICustomerService {

    List<Customer> findAll();

    Customer findOneById(Integer id);

    Customer findByEmailAndPassword(String email, String password);

    void createCustomer(Customer customer);

    void updateCustomer(Customer customer);

    void deleteCustomer(Customer customer);
}
