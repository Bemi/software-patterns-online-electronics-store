package com.example.core.service;

import com.example.core.entities.Administrator;

import java.util.List;

public interface IAdministratorService {

    List<Administrator> findAll();

    Administrator findOneById(Integer id);

    Administrator findByEmailAndPassword(String email, String password);

    void createAdministrator(Administrator administrator);

    void updateAdministrator(Administrator administrator);

    void deleteAdministrator(Administrator administrator);
}
