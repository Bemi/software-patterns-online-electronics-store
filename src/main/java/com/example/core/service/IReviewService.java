package com.example.core.service;

import com.example.core.entities.Customer;
import com.example.core.entities.Review;
import com.example.core.entities.Stock;

import java.util.List;

public interface IReviewService {

    List<Review> findAll();

    Review findOneById(Integer id);

    List<Review> findByStock(Stock stock);

    List<Review> findByCustomer(Customer customer);

    void createReview(Review review);

    void updateReview(Review review);

    void deleteReview(Review review);
}
