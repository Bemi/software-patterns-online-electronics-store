package com.example.core.service.impl;

import com.example.core.entities.Customer;
import com.example.core.entities.Review;
import com.example.core.entities.Stock;
import com.example.core.repository.ReviewRepository;
import com.example.core.service.IReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ReviewService implements IReviewService {

    ReviewRepository reviewRepository;

    @Autowired
    public void setReviewRepository(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }


    @Override
    public List<Review> findAll() {
        return reviewRepository.findAll();
    }

    @Override
    public Review findOneById(Integer id) {
        return reviewRepository.findOne(id);
    }

    @Override
    public List<Review> findByStock(Stock stock) {
        return reviewRepository.findByStock(stock);
    }

    @Override
    public List<Review> findByCustomer(Customer customer) {
        return reviewRepository.findByCustomer(customer);
    }

    @Override
    public void createReview(Review review) {
        reviewRepository.save(review);
    }

    @Override
    public void updateReview(Review review) {
        reviewRepository.save(review);
    }

    @Override
    public void deleteReview(Review review) {
        reviewRepository.delete(review);
    }
}