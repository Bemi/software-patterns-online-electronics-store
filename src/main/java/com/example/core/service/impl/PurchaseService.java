package com.example.core.service.impl;

import com.example.core.entities.Customer;
import com.example.core.entities.Purchase;
import com.example.core.entities.Stock;
import com.example.core.repository.PurchaseRepository;
import com.example.core.service.IPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PurchaseService implements IPurchaseService {

    PurchaseRepository purchaseRepository;

    @Autowired
    public void setPurchaseRepository(PurchaseRepository purchaseRepository) {
        this.purchaseRepository = purchaseRepository;
    }


    @Override
    public List<Purchase> findAll() {
        return purchaseRepository.findAll();
    }

    @Override
    public Purchase findOneById(Integer id) {
        return purchaseRepository.findOne(id);
    }

    @Override
    public  List<Purchase> findByStock(Stock stock) {
        return purchaseRepository.findByStock(stock);
    }

    @Override
    public  List<Purchase> findByCustomer(Customer customer) {
        return purchaseRepository.findByCustomer(customer);
    }

    @Override
    public void createPurchase(Purchase purchase) {
        purchaseRepository.save(purchase);
    }

    @Override
    public void updatePurchase(Purchase purchase) {
        purchaseRepository.save(purchase);
    }

    @Override
    public void deletePurchase(Purchase purchase) {
        purchaseRepository.delete(purchase);
    }
}