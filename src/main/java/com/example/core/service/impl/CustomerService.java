package com.example.core.service.impl;

import com.example.core.entities.Customer;
import com.example.core.repository.CustomerRepository;
import com.example.core.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
@Transactional
public class CustomerService implements ICustomerService {

    private static AtomicReference<CustomerService> INSTANCE = new AtomicReference<CustomerService>();

    public CustomerService() {
        final CustomerService previous = INSTANCE.getAndSet(this);
        if(previous != null)
            throw new IllegalStateException("Second singleton " + this + " created after " + previous);
    }

    public static CustomerService getInstance() {
        return INSTANCE.get();
    }

    CustomerRepository customerRepository;

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    @Override
    public Customer findOneById(Integer id) {
        return customerRepository.findById(id);
    }

    @Override
    public Customer findByEmailAndPassword(String email, String password) {
        return customerRepository.findByEmailAndPassword(email,password);
    }

    @Override
    public void createCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void updateCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void deleteCustomer(Customer customer) {
        customerRepository.delete(customer);
    }
}