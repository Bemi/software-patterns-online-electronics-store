package com.example.core.service.impl;

import com.example.core.entities.Stock;
import com.example.core.repository.StockRepository;
import com.example.core.service.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class StockService implements IStockService {

    StockRepository stockRepository;

    @Autowired
    public void setStockRepository(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @Override
    public List<Stock> findAll() {
        return stockRepository.findAll();
    }

    @Override
    public Stock findOneById(Integer id) {
        return stockRepository.findById(id);
    }

    @Override
    public Stock findByTitle(String title) {
        return stockRepository.findByTitle(title);
    }

    @Override
    public List<Stock> findByCategory(String category) {
        return stockRepository.findByCategory(category);
    }

    @Override
    public List<Stock> findByManufacturer(String manufacturer) {
        return stockRepository.findByManufacturer(manufacturer);
    }

    @Override
    public void createStock(Stock stock) {
        stock.setUploadDate(new Date());
        stockRepository.save(stock);
    }

    @Override
    public void updateStock(Stock stock) {
        stockRepository.save(stock);
    }

    @Override
    public void deleteStock(Stock stock) {
        stockRepository.delete(stock);
    }
}