package com.example.core.service.impl;

import com.example.core.entities.Administrator;
import com.example.core.repository.AdministratorRepository;
import com.example.core.service.IAdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
@Transactional
public class AdministratorService implements IAdministratorService {

    private static AtomicReference<AdministratorService> INSTANCE = new AtomicReference<AdministratorService>();

    public AdministratorService() {
        final AdministratorService previous = INSTANCE.getAndSet(this);
        if(previous != null)
            throw new IllegalStateException("Second singleton " + this + " created after " + previous);
    }

    AdministratorRepository administratorRepository;

    @Autowired
    public void setAdministratorRepository(AdministratorRepository administratorRepository) {
        this.administratorRepository = administratorRepository;
    }

    public static AdministratorService getInstance() {
        return INSTANCE.get();
    }
    @Override
    public List<Administrator> findAll() {
        return administratorRepository.findAll();
    }

    @Override
    public Administrator findOneById(Integer id) {
        return administratorRepository.findOne(id);
    }

    @Override
    public Administrator findByEmailAndPassword(String email, String password) {
        return administratorRepository.findByEmailAndPassword(email,password);
    }

    @Override
    public void createAdministrator(Administrator administrator) {
        administratorRepository.save(administrator);
    }

    @Override
    public void updateAdministrator(Administrator administrator) {
        administratorRepository.save(administrator);
    }

    @Override
    public void deleteAdministrator(Administrator administrator) {
        administratorRepository.delete(administrator);
    }
}