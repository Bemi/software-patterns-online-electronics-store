package com.example.core.service;

import com.example.core.entities.Stock;

import java.util.List;

public interface IStockService {

    List<Stock> findAll();

    Stock findOneById(Integer id);

    Stock findByTitle(String title);

    List<Stock> findByCategory(String category);

    List<Stock> findByManufacturer(String manufacturer);

    void createStock(Stock stock);

    void updateStock(Stock stock);

    void deleteStock(Stock stock);
}
