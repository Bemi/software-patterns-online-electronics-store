package com.example.core.repository;

import com.example.core.entities.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StockRepository extends JpaRepository<Stock, Integer> {

    Stock findById(Integer id);

    Stock findByTitle(String title);

    List<Stock> findByCategory(String category);

    List<Stock> findByManufacturer(String manufacturer);
}
