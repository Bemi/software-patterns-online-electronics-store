package com.example.core.repository;

import com.example.core.entities.Customer;
import com.example.core.entities.Review;
import com.example.core.entities.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Integer> {

    Review findById(Integer id);

    List<Review> findByStock(Stock stock);

    List<Review> findByCustomer(Customer customer);
}
