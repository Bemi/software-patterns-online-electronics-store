package com.example.core.repository;

import com.example.core.entities.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

    Administrator findById(Integer id);

    Administrator findByEmailAndPassword(String email, String password);

}
