package com.example.core.repository;

import com.example.core.entities.Customer;
import com.example.core.entities.Purchase;
import com.example.core.entities.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {

    Purchase findById(Integer id);

    List<Purchase> findByStock(Stock stock);

    List<Purchase> findByCustomer(Customer customer);
}
