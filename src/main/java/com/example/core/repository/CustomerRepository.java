package com.example.core.repository;

import com.example.core.entities.Customer;
import com.example.core.entities.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    Customer findById(Integer id);

    Customer findByEmailAndPassword(String email, String password);

    Customer findBySubscriber(boolean subscriber);
}
