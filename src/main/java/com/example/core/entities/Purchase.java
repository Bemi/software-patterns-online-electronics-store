package com.example.core.entities;

import javax.persistence.*;

@Entity
public class Purchase {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    private int quantity;

    private Double pricePaid;

    private boolean paid;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Stock stock;

    public Purchase() {
    }

    public Purchase(int quantity, Double pricePaid, boolean paid, Customer customer, Stock stock) {
        this.quantity = quantity;
        this.pricePaid = pricePaid;
        this.paid = paid;
        this.customer = customer;
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Double getPricePaid() {
        return pricePaid;
    }

    public void setPricePaid(Double pricePaid) {
        this.pricePaid = pricePaid;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", pricePaid=" + pricePaid +
                ", paid=" + paid +
                ", customer=" + customer +
                ", stock=" + stock +
                '}';
    }
}

