package com.example.core.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Stock {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    private String title;
    private String manufacturer;
    private String price;
    private String category;
    private int stockLevel;
    private String stockImageUrl;
    private Date uploadDate;
    private int averageRating;

    @Lob
    @Column(length=10000)
    private String description;



    public Stock() {
    }

    public Stock(String title, String manufacturer, String price, String category, int stockLevel, String stockImageUrl, Date uploadDate, int averageRating, String description) {
        this.title = title;
        this.manufacturer = manufacturer;
        this.price = price;
        this.category = category;
        this.stockLevel = stockLevel;
        this.stockImageUrl = stockImageUrl;
        this.uploadDate = uploadDate;
        this.averageRating = averageRating;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getStockLevel() {
        return stockLevel;
    }

    public void setStockLevel(int stockLevel) {
        this.stockLevel = stockLevel;
    }

    public String getStockImageUrl() {
        return stockImageUrl;
    }

    public void setStockImageUrl(String stockImageUrl) {
        this.stockImageUrl = stockImageUrl;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public int getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(int averageRating) {
        this.averageRating = averageRating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", price='" + price + '\'' +
                ", category='" + category + '\'' +
                ", stockLevel=" + stockLevel +
                ", stockImageUrl='" + stockImageUrl + '\'' +
                ", uploadDate=" + uploadDate +
                ", averageRating=" + averageRating +
                ", description='" + description + '\'' +
                '}';
    }
}

