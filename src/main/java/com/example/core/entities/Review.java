package com.example.core.entities;

import javax.persistence.*;

@Entity
public class Review {


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    @Lob
    @Column(length=1000)
    private String comment;

    private int rating;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Stock stock;

    public Review() {
    }

    public Review(String comment, int rating, Customer customer, Stock stock) {
        this.comment = comment;
        this.rating = rating;
        this.customer = customer;
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", comment='" + comment + '\'' +
                ", rating=" + rating +
                ", customer=" + customer +
                ", stock=" + stock +
                '}';
    }
}

