package com.example.patterns.observer.concrete;

import com.example.core.entities.Customer;
import com.example.patterns.observer.Observer;
import com.example.patterns.observer.Subject;
import com.example.core.service.impl.CustomerService;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SubscriberObserver extends Observer {


    public SubscriberObserver(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update()  {
        List<Customer> customerList = CustomerService.getInstance().findAll();

        for (Customer c: customerList) {
            if(c.isSubscriber()) {
                Properties mailServerProperties;
                Session getMailSession;
                MimeMessage generateMailMessage;

                InetAddress ip;
                String hostname = "";
                try {
                    ip = InetAddress.getLocalHost();
                    hostname = ip.getHostAddress();
                } catch (UnknownHostException e) {

                    e.printStackTrace();
                }

                mailServerProperties = System.getProperties();
                mailServerProperties.put("mail.smtp.port", "587");
                mailServerProperties.put("mail.smtp.auth", "true");
                mailServerProperties.put("mail.smtp.starttls.enable", "true");
                System.out.println("Mail Server Properties have been setup successfully..");

                // Step2
                System.out.println("\n\n 2nd ===> get Mail Session..");
                getMailSession = Session.getDefaultInstance(mailServerProperties, null);
                generateMailMessage = new MimeMessage(getMailSession);
                try {
                    generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(c.getEmail()));
                    generateMailMessage.setSubject("New item in stock");
                    String emailBody = "New " + subject.getState().getTitle() + " added to stock, visit our site to get yours at a discounted price.";
                    generateMailMessage.setContent(emailBody, "text/html");
                    System.out.println("Mail Session has been created successfully..");

                    // Step3
                    System.out.println("\n\n 3rd ===> Get Session and Send mail");
                    Transport transport = getMailSession.getTransport("smtp");

                    // Enter your correct gmail UserID and Password
                    // if you have 2FA enabled then provide App Specific Password
                    transport.connect("smtp.gmail.com", "trustsecuredinc@gmail.com", "TrustSecuredInc!");
                    transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
                    transport.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
