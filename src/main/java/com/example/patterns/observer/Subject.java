package com.example.patterns.observer;

import com.example.core.entities.Stock;

import java.util.ArrayList;
import java.util.List;

public class Subject {

    private List<Observer> observers = new ArrayList<Observer>();
    private Stock stock;

    public Stock getState() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
        notifyAllObservers();
    }

    public void attach(Observer observer){
        observers.add(observer);
    }

    public void notifyAllObservers(){
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
