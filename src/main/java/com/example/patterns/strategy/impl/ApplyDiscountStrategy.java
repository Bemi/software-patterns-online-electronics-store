package com.example.patterns.strategy.impl;

import com.example.patterns.strategy.IApplyDiscountStrategy;

public class ApplyDiscountStrategy implements IApplyDiscountStrategy {

    @Override
    public double applyDiscount(double price) {
        return (price * .95);
    }

}
