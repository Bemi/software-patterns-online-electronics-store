package com.example.patterns.strategy;

/**
 * Created by gbemigaadeosun on 27/03/2017.
 */
public interface IApplyDiscountStrategy {

    double applyDiscount(double price);

}